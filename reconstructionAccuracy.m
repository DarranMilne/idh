function [Difference] = reconstructionAccuracy(objectCoord, final_posn)
objectSize=size(objectCoord,1);
outputSize=size(final_posn,1);

if objectSize>=outputSize
    comparison=false(1,outputSize);
    for i=1:outputSize
        comparison(i)=ismember(objectCoord(i,:),final_posn,'rows');
    end
    
    Difference=abs(sum(comparison)-size(comparison,2))/size(comparison,2)*100; % Need to remove reference point
    fprintf(' the percentage difference is %.1f%%\n',Difference);
end

if objectSize<outputSize
    comparison=false(1,objectSize);
    for i=1:objectSize
        comparison(i)=ismember(final_posn(i,:),objectCoord,'rows');
    end
    
    Difference=abs(sum(comparison)-size(comparison,2))/size(comparison,2)*100; % Need to remove reference point
    fprintf(' the percentage difference is %.2f %%\n',Difference);
end

