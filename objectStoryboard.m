function varargout = objectStoryboard(varargin)
% OBJECTSTORYBOARD MATLAB code for objectStoryboard.fig
%      OBJECTSTORYBOARD, by itself, creates a new OBJECTSTORYBOARD or raises the existing
%      singleton*.
%
%      H = OBJECTSTORYBOARD returns the handle to a new OBJECTSTORYBOARD or the handle to
%      the existing singleton*.
%
%      OBJECTSTORYBOARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OBJECTSTORYBOARD.M with the given input arguments.
%
%      OBJECTSTORYBOARD('Property','Value',...) creates a new OBJECTSTORYBOARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before objectStoryboard_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to objectStoryboard_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help objectStoryboard

% Last Modified by GUIDE v2.5 13-Jul-2015 09:50:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @objectStoryboard_OpeningFcn, ...
                   'gui_OutputFcn',  @objectStoryboard_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before objectStoryboard is made visible.
function objectStoryboard_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to objectStoryboard (see VARARGIN)

% Load up parameters structure as a global variable
parameterFile;

% Choose default command line output for objectStoryboard
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes objectStoryboard wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = objectStoryboard_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadPushButton.
function loadPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global parameters;

[filename, pathname, filterIndex] = uigetfile();
if filterIndex ~= 0
    matFile = fullfile(pathname, filename);
    loader = load(matFile);
    fn = fieldnames(loader);
    if length(fn) ~= 1
        errordlg('Error: .mat file must contain 1 matrix, size n x 3');
        return;
    else
        pointCloud = loader.(fn{1});
        if size(pointCloud, 2) ~= 3
            errordlg('Error: .mat file must contain 1 matrix, size n x 3');
            return;
        else          
            % Add reference points to the point cloud           
            refDistance=parameters.refDistance;
            holoHalfWidth=parameters.holoHalfWidth;
            startDistance=parameters.startDistance;
            endDistance=parameters.endDistance;
            distanceRange = endDistance-startDistance;
            noDistances = distanceRange/parameters.zPlanes+1;
            refPlanes=parameters.refPlanes;
            
            for z=1:(noDistances-1)/refPlanes
                dist=startDistance+z*refPlanes;
                referencePoint=[holoHalfWidth-refDistance,holoHalfWidth-refDistance,dist];
                pointCloud=[pointCloud;referencePoint];  % Add a reference peak
            end
            
            % update GUI handles
            handles.pointCloud = pointCloud;
            guidata(hObject, handles);
            plotPointCloud(hObject, eventdata, handles);
        end
    end
end


% --- Executes on button press in createPushButton.
function createPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to createPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global parameters;
defSize = num2str(parameters.holoHalfWidth);
defZMin = num2str(parameters.distance1);
defZMax = num2str(parameters.distance2);


prompt = {'Number of digits:', 'x Lower', 'x Upper', 'y Lower', 'y Upper', 'z Lower', 'z Upper'};
dlg_title = 'Create 3D Object';
num_lines = 1;
def = {'20', ['-' defSize], defSize, ['-' defSize], defSize, defZMin, defZMax};
PCoptions = inputdlg(prompt, dlg_title, num_lines, def);

numberOfDigits = str2double(PCoptions{1});
xLims = [str2double(PCoptions{2}), str2double(PCoptions{3})];
yLims = [str2double(PCoptions{4}), str2double(PCoptions{5})];
zLims = [str2double(PCoptions{6}), str2double(PCoptions{7})];

pointCloud = structuredRandomPointCloudGenerator(numberOfDigits, xLims, yLims, zLims);

% update GUI handles
handles.pointCloud = pointCloud;
guidata(hObject, handles);
plotPointCloud(hObject, eventdata, handles);

% --- Plots point cloud on main axes
function plotPointCloud(hObject, eventdata, handles)
data = handles.pointCloud;
axes(handles.mainAxes);
scatter3(data(:,1), data(:,2), data(:,3), 'MarkerFaceColor', 'r');

axes(handles.objectAxes);
scatter3(data(:,1), data(:,2), data(:,3), 'MarkerFaceColor', 'r');

rotate3d;




% --- Executes on button press in editParameters.
function editParameters_Callback(hObject, eventdata, handles)
% hObject    handle to editParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global parameters;

% default values
defLambda = num2str(parameters.lambda);
defObjectAmplitude = num2str(parameters.objectAmp);
defHoloHalfWidth = num2str(parameters.holoHalfWidth);
defDistance1 = num2str(parameters.startDistance);
defDistance2 = num2str(parameters.endDistance);
defZPlanes = num2str(parameters.zPlanes);
refPlanes = num2str(parameters.refPlanes);

% populate input dialog box
prompt = {'Lambda:', 'Object Amplitude:', 'Hologram Half-Width:', 'Z-Distance 1:', 'Z-Distance 2:', 'Z-Plane Spacing:', 'Reference Plane Spacing:'};
dlg_title = 'Edit Parameters';
num_lines = 1;
def = {defLambda, defObjectAmplitude, defHoloHalfWidth, defDistance1, defDistance2, defZPlanes, refPlanes};
newParameters = inputdlg(prompt, dlg_title, num_lines, def);

% update parameter values
if ~isempty(newParameters)
    parameters.lambda = str2double(newParameters{1});
    parameters.objectAmp = str2double(newParameters{2});
    parameters.holoHalfWidth = str2double(newParameters{3});
    parameters.startDistance = str2double(newParameters{4});
    parameters.endDistance = str2double(newParameters{5});
    parameters.zPlanes = str2double(newParameters{6});
    parameters.refPlanes = str2double(newParameters{7});

    parameters.holoSamples = parameters.holoHalfWidth*2;              
end

% --- Executes on button press in calculateHologramButton.
function calculateHologramButton_Callback(hObject, eventdata, handles)
% hObject    handle to calculateHologramButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
holo = computeHologram(handles.pointCloud);

axes(handles.mainAxes);
imagesc(abs(holo));

axes(handles.hologramAxes);
imagesc(abs(holo));

% update GUI
handles.holo = holo;
handles.originalHolo = holo;
guidata(hObject, handles);


% --- Executes on button press in saveHologramButton.
function saveHologramButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveHologramButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hologram = handles.holo;

[compressedHolo, compressedSize] = compressHologram(hologram, 0, 'png');

% update GUI
handles.holo = compressedHolo;
guidata(hObject, handles);

msgbox(sprintf('Saved hologram is %.1f kB\n', compressedSize), 'Hologram Saved');


% --- Executes on button press in restoreButton.
function restoreButton_Callback(hObject, eventdata, handles)
% hObject    handle to restoreButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
reconstruction = restoreSlices(handles.holo);

set(handles.zSlider, 'Min', 1);
set(handles.zSlider, 'Max', size(reconstruction, 3));
initialZValue = round(size(reconstruction, 3)/2);
set(handles.zSlider, 'Value', initialZValue);

% update GUI
handles.reconstruction = reconstruction;
guidata(hObject, handles);
zSlider_Callback(hObject, eventdata, handles);


% --- Executes on button press in findPeaksButton.
function findPeaksButton_Callback(hObject, eventdata, handles)
% hObject    handle to findPeaksButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
restoredPeaks = findPeaksFromSlices(handles.reconstruction);

% Plot restored peaks, neatly
axes(handles.mainAxes);
scatter3(restoredPeaks(:,1), restoredPeaks(:,2), restoredPeaks(:,3), 'MarkerEdgeColor','k','MarkerFaceColor','r');
axis equal;
lim = axis;
lim(1:2:5) = lim(1:2:5) - 5;
lim(2:2:6) = lim(2:2:6) + 5;
axis(lim);

axes(handles.restoredAxes);
scatter3(restoredPeaks(:,1), restoredPeaks(:,2), restoredPeaks(:,3), 'MarkerEdgeColor','k','MarkerFaceColor','r');
axis equal;
lim = axis;
lim(1:2:5) = lim(1:2:5) - 5;
lim(2:2:6) = lim(2:2:6) + 5;
axis(lim);


% --- Executes on slider movement.
function zSlider_Callback(hObject, eventdata, handles)
% hObject    handle to zSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
z = round(get(handles.zSlider, 'Value'));
recon = handles.reconstruction(:,:,z);

axes(handles.mainAxes);
imagesc(abs(recon)/max(max(max(abs(recon)))));
set(handles.zText, 'String', sprintf('%d', z));


% --- Executes during object creation, after setting all properties.
function zSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in loadIRCheckbox.
function loadIRCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to loadIRCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of loadIRCheckbox
% nothing. 


% --- Executes on button press in filterPushButton.
function filterPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to filterPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkerboard = get(handles.checkerboardCheck, 'Value');
inner = get(handles.innerCircleCheck, 'Value');
outer = get(handles.outerCircleCheck, 'Value');

holo = handles.originalHolo;

% Compute masks
if checkerboard
    holo(1:2:end, 1:2:end) = 0;
end

h = size(holo,1);
w = size(holo,2);
centerH = floor(h/2);
centerW = floor(w/2);
[W,H] = meshgrid(1:w,1:h);
Ri = h/8;
Ro = 3*h/8;

if inner
    mask = sqrt((W-centerW).^2 + (H-centerH).^2) > Ri;
    holo(~mask) = 0;
end

if outer
    mask = sqrt((W-centerW).^2 + (H-centerH).^2) < Ro;
    holo(~mask) = 0;
end

% Show threshold hologram
axes(handles.mainAxes);
imagesc(abs(holo));

% Update GUI
handles.holo = holo;
handles.originalHolo = holo;
guidata(hObject, handles);


% --- Executes on button press in checkerboardCheck.
function checkerboardCheck_Callback(hObject, eventdata, handles)
% hObject    handle to checkerboardCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkerboardCheck
updateHologram(hObject, eventdata, handles);

% --- Executes on button press in innerCircleCheck.
function innerCircleCheck_Callback(hObject, eventdata, handles)
% hObject    handle to innerCircleCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of innerCircleCheck
updateHologram(hObject, eventdata, handles);

% --- Executes on button press in outerCircleCheck.
function outerCircleCheck_Callback(hObject, eventdata, handles)
% hObject    handle to outerCircleCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of outerCircleCheck
updateHologram(hObject, eventdata, handles);

% --- Update the filters on the hologram
function updateHologram(hObject, eventdata, handles)
% Creat variables
holo = handles.originalHolo;

h = size(holo,1);
w = size(holo,2);
centerH = floor(h/2);
centerW = floor(w/2);
[W,H] = meshgrid(1:w,1:h);

% Apply filters
if get(handles.checkerboardCheck, 'Value')
    holo(1:2:end, 1:2:end) = 0;
end

if get(handles.innerCircleCheck, 'Value')
    radius = 0.33 * size(holo,2)/2;
    mask = sqrt((W-centerW).^2 + (H-centerH).^2) < radius;
    holo(mask) = 0;
end

if get(handles.outerCircleCheck, 'Value')
    radius = 0.75 * size(holo,2)/2;
    mask = sqrt((W-centerW).^2 + (H-centerH).^2) > radius;
    holo(mask) = 0;
end

% Show threshold hologram
axes(handles.mainAxes);
imagesc(abs(holo));

% Update GUI
handles.holo = holo;
guidata(hObject, handles);
