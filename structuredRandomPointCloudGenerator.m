function objectCoords = structuredRandomPointCloudGenerator(numberOfDigits, xLim, yLim, zLim)
%
% Generate a point cloud with many points. Points are arranged into
% recognisable digits, but each digit is placed and rotated randomly in
% x-y space. z is restricted to certain planes
%
%

scale = 10;
global parameters;

%% Set up shape coordinates, centred at 0
digits{1} = [-4 6;-4 4;-4 2;-4 0;-4 -2;-4 -4;-4 -6;-2 -6;0 -6;2 -6;4 -6;4 -4;4 -2;4 0;4 2;4 4;4 6;2 6;0 6;-2 6]; % zero
digits{2} = [-4 6;-2 6;0 6;2 6;4 6;4 4;4 2;4 0;2 0;0 0;-2 0;-4 0;-4 -2;-4 -4;-4 -6;-2 -6;0 -6;2 -6;4 -6];     % two
digits{3} = [-4 6;-2 6;0 6;2 6;4 6;4 4;4 2;4 0;2 0;0 0;-2 0;-4 0;4 -2;4 -4;-4 -6;-2 -6;0 -6;2 -6;4 -6]; % three
digits{4} = [-4 6;-2 6;0 6;2 6;4 6;-4 4;-4 2;4 0;2 0;0 0;-2 0;-4 0;4 -2;4 -4;4 -6;-2 -6;0 -6;2 -6; -4 -6];  % five
digits{5} = [-4 6;-4 4;-4 2;-4 0;-4 -2;-4 -4;-4 -6;-2 -6;0 -6;2 -6;4 -6;4 -4;4 -2;4 0;4 6;2 6;0 6;-2 6; -2 0; 0 0; 2 0];  % six
digits{6} = [-4 6;-4 4;-4 2;-4 0;-4 -2;-4 -4;-4 -6;-2 -6;0 -6;2 -6;4 -6;4 -4;4 -2;4 0;4 2;4 4;4 6;2 6;0 6;-2 6; -2 0; 0 0; 2 0]; % eight
digits{7} = [-4 6;-2 6;0 6;2 6;4 6;4 4;4 2;4 0;2 0;0 0;-2 0;-4 0;4 -2;4 -4;-4 -6;-2 -6;0 -6;2 -6;4 -6;-4 4; -4 2];   % nine

if nargin == 1
    xLim = [-parameters.holoHalfWidth, +parameters.holoHalfWidth];
    yLim = [-parameters.holoHalfWidth, +parameters.holoHalfWidth];
    zLim = [parameters.distance1, parameters.distance2];
end

%% Plot numbers (just to test that they look correct)
% while 1
%     for i = 1:length(digits)
%         digit = digits{i};
%         scatter(digit(:,1), digit(:,2), 'MarkerEdgeColor','k','MarkerFaceColor','b');
%         axis([-10 10 -10 10]);
%         drawnow;
%         pause(0.3);
%     end
% end

%% Create some random lists
zPositions = randi(zLim/10, numberOfDigits, 1)*10;    % list of random 
rotations = randi([-80, 80], numberOfDigits, 1);  % list of random rotations from -80 to 80 degrees
transX = randi(xLim, numberOfDigits, 1);
transY = randi(yLim, numberOfDigits, 1);
chooseDigit = randi(length(digits), numberOfDigits, 1);

%% Create output
objectCoords = [];  % Should really preallocate, but that would be a bit complicated for this function so.. just have it slow, oh well. 

for i = 1:numberOfDigits
    % Load in a random digit
    digit = scale * digits{chooseDigit(i)}; 
    
    % Rotate it
    rAngle = rotations(i);
    rotationMatrix = [cosd(rAngle), -sind(rAngle); sind(rAngle), cosd(rAngle)];
    digit = digit * rotationMatrix;    

    % Translate it
    digit(:,1) = digit(:,1) + transX(i); 
    digit(:,2) = digit(:,2) + transY(i); 
    
    % Set a z position
    z = zPositions(i);
    digit(:,3) = z;
    
    % Pop digit into objectCoords
    objectCoords = [objectCoords; digit]; 
end