%% Default parameters we are using in hologram computation/restoration
global parameters;

%% Wavelength and wavevector
parameters.lambda = 2*10^-3;                                 % Define wavelength
parameters.k = 2*pi/parameters.lambda;                            % Define wavevector    

%% Parameters for creating the hologram
parameters.objectAmp = 1;                               % Define amplitude of object
parameters.holoHalfWidth = 300;                         % Hologram area.
parameters.holoSamples = parameters.holoHalfWidth*2;    % Set samples per dimension: Larger gives more delta like replay field, but takes more computation.

%% Parameters for restoring slices of the 3D-object
parameters.startDistance=1000;             % Less than 5 doesn't really work
parameters.endDistance=1025;              % How far the calculate the field from the hologram plane. Larger takes more time.
parameters.zPlanes = 1;                 % Distance between z planes
parameters.refPlanes=5;                 % Distance apart of reference planes. Set to zero to remove.
parameters.zeroPaddingMultiplier=2;     % How much to zero pad the hologram
%% Peak finding parameters

parameters.refDistance=30;             % How far the reference points are from the edge
parameters.threshold=0.85;             % Raise to give less points detected    

% %% P28 Object Coordinates
% objectCoord = [-20,-50,20; -10,-50,20; 0,-50,20; 10,-50,20; 20,-50,20; -20,-40,20;-20,-30,20; -10,-30,20;0,-30,20; 0,-40,20;
%     -20,-10,20;-20,0,20;-20,10,20;-10,10,20;0,10,20;0,0,20;0,-10,20;10,-10,20;20,-10,20;20,0,20;20,10,20;
%     -20,30,30;-20,40,30;-20,50,30;-10,50,30;0,50,30;0,40,30;0,30,30; -10,30,30; 10,30,30;20,30,30;20,40,30;20,50,30;10,50,30];% P28 letters
% for i=1:17
%         objectCoord=[objectCoord;80,80-(i-1)*10,20];
%         objectCoord=[objectCoord;-80,80-(i-1)*10,20];
% end
% for i=1:15
%         objectCoord=[objectCoord;-70+(i-1)*10,80,20];
%         objectCoord=[objectCoord;-70+(i-1)*10,-80,20];
% end
% newPoints = [-20,-50,40; -10,-50,40; 0,-50,40; 10,-50,40; 20,-50,40; -20,-40,40;-20,-30,40; -10,-30,40;0,-30,40; 0,-40,40;
%     -20,-10,50;-20,0,50;-20,10,50;-10,10,50;0,10,50;0,0,50;0,-10,50;10,-10,50;20,-10,50;20,0,50;20,10,50;
%     -20,30,60;-20,40,60;-20,50,60;-10,50,60;0,50,60;0,40,60;0,30,60; -10,30,60; 10,30,60;20,30,60;20,40,60;20,50,60;10,50,60];% P28 letters
% objectCoord=[objectCoord;newPoints];

