function [restoredPointCloud] = restoreSlices(holo)
%
% This function takes a hologram computed by computeHologram.m, and returns
% a matrix (x by y by z), where z is the distance from the hologram plane.
% Each z plane can then be viewed either as a movie (using implay) or using 
% a slider to move through different z's. 

%% Load the parameters for our system
global parameters;

%% Preallocate some matricies
distanceRange = parameters.endDistance-parameters.startDistance;
noDistances = distanceRange/parameters.zPlanes+1;
startDistance=parameters.startDistance;
zPlanes=parameters.zPlanes;
holoHW = parameters.holoHalfWidth;
holoS = parameters.holoSamples;
k = parameters.k;
zeroPadMult=parameters.zeroPaddingMultiplier;
middlePoint=holoS*zeroPadMult/2+1;

noXYPoints=holoS*zeroPadMult+1;
recon=zeros(noXYPoints, noXYPoints, noDistances);
impulseResponse=zeros(noXYPoints, noXYPoints, noDistances);

%% Compute the impulse response
[planeX, planeY] = meshgrid(linspace(-holoHW*zeroPadMult,holoHW*zeroPadMult, noXYPoints),...
    linspace(-holoHW*zeroPadMult,holoHW*zeroPadMult, noXYPoints));

for z = 1:noDistances %parameters.distance1:parameters.zPlanes:parameters.distance2
    dist=startDistance+(z-1)*zPlanes;
    r=sqrt(planeX.^2+planeY.^2+dist^2);
    impulseResponse(:,:,z) = 1i*k.*dist./(2*pi*r.^2).*exp(-1i*k*r);%.*(1 + 1./(1i*k*r));
end

%% Reconstruct the hologram
holo=padarray(holo,[(holoHW)*(zeroPadMult-1),holoHW*(zeroPadMult-1)]);
holo=fft2(holo);
for n=1:noDistances
    IR = fft2(impulseResponse(:,:,n));
    recon(:,:,n) = fftshift(ifft2(IR.*holo)); % Convolution is a multiplication in Fourier space
end

%% Return the reconstructed point cloud

recon=recon(middlePoint-holoHW:middlePoint+holoHW,middlePoint-holoHW:middlePoint+holoHW,:);
restoredPointCloud = recon;

end

