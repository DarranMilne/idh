function finalPosition = findPeaksFromSlices(recon)
%
% Functionalised version of George Robert's code to find peaks in a
% 'reconstructed' point cloud. Returns an n x 3 matrix containing the
% coordinates of the peaks in the restored point cloud. 
%

%% Load in parameters (all we really need is distance1 and distance2)
global parameters;

%% Make some useful variables
distanceRange = parameters.endDistance-parameters.startDistance;
noDistances = distanceRange/parameters.zPlanes+1;
holoHW = parameters.holoHalfWidth;
holoS = parameters.holoSamples;
refDistance=parameters.refDistance;
threshold=parameters.threshold;
refPlanes=parameters.refPlanes;

%% Threshold reconstructed values
recon=abs(recon)/max(max(max(abs(recon))));

arb=refPlanes;
for z=1:noDistances
    minPeak=threshold*abs(recon(holoHW*2-refDistance,holoHW*2-refDistance,z+arb));
    recon(holoHW*2-refDistance,holoHW*2-refDistance,z)=0;
    temp=abs(recon(:,:,z));
    temp(temp<minPeak)=0;
    temp(temp>=minPeak)=1;
    recon(:,:,z)=temp;
    if arb==0;
        arb=refPlanes;
    end
    arb=arb-1;
end

%% Allocate the peaks into a matrix
idx = find(recon);
[X,Y,Z] = ind2sub(size(recon), idx);

finalPosition(:,1)=round(X/holoS*holoHW*2 - holoHW); % Calculates the actual x values
finalPosition(:,2)=round(Y/holoS*holoHW*2 - holoHW); % Calculates the actual y values
finalPosition(:,3)=(Z-1)*parameters.zPlanes + parameters.startDistance; % Calculate the actual z values

%% Old threshold
% 
% for i=1:No_distances
%     maxValue=max(max(abs(recon(:,:,i))));
%     thresholdValue = 0.5*maxValue;
%     tmp = abs(recon(:,:,i));
%     tmp(tmp<thresholdValue) = 0;
%     tmp(tmp<minPeak)=0;
% 	tmp(tmp>=minPeak)=1;
%     tmp(tmp>=thresholdValue)=1;
%     recon(:,:,i) = tmp;
% end
% 
% % Finds the peaks and stores them as a matrix of x-y,x-y,x-y,x-y as a function of z
% % filter=zeros(6);
% % filter(3:4,3:4)=1;            % The response is squares, so attempt a new filter?
% for z=1:No_distances
%     b=FastPeakFind(log(abs(recon(:,:,z))),0, fspecial('gaussian', [5 5],1));  % Outputs all peaks as one row vector for a specific distance
%     b=transpose(b);
%     c(z,1:size(b,2))=b;                 % Organises the row vectors into a matrix with row value corresponding to a certain distance
% end
% 
% k=1;
% for m=1:size(c,2)/2          % Loops through each of the peaks at a distance
%     for i=1:size(c,1)       % Loops through distances
%         if c(i,2*m-1)~=0     % Ensures only actual peaks are looped over
%             x(k)=c(i,2*m-1);
%             y(k)=c(i,2*m);
%             finalzposn(k)=i;
%             k=k+1;
%         end
%     end
% end
% 
% finalPosition=[x;y;finalzposn];                % Concatenate into one matrix for easy viewing
% finalPosition=finalPosition';
% finalPosition=unique(finalPosition,'rows');               % Deletes duplicates
% 
% % This part of the code keeps only points on a regular grid: z grid reduntant now because code only calculates certain z distances.
% %%%%% Perhaps re-implement on the x-y grid to reduce noise?
% 
% xReconMinSpacing=parameters.sampling*parameters.XYgridSpacing;
% middlePoint=holoS/2;
% xNoPointsEitherSide=round(holoS/4/parameters.XYgridSpacing);
% xAllowedGrid=middlePoint-xReconMinSpacing*xNoPointsEitherSide:xReconMinSpacing:middlePoint+xReconMinSpacing*xNoPointsEitherSide;
% 
% yObjectMinSpacing=parameters.sampling*parameters.XYgridSpacing;
% yReconMinSpacing=parameters.sampling*yObjectMinSpacing;
% yNoPointsEitherSide=round(holoS/4/parameters.XYgridSpacing);
% yAllowedGrid=middlePoint-yReconMinSpacing*yNoPointsEitherSide:yReconMinSpacing:middlePoint+yReconMinSpacing*yNoPointsEitherSide;
% 
% XonGrid=ismember(finalPosition(:,1),xAllowedGrid);
% YonGrid=ismember(finalPosition(:,2),yAllowedGrid);
% 
% for i=1:size(finalPosition,1)
%     if XonGrid(i)==0
%         finalPosition(i,:)=0;
%     end
% end
% for i=1:size(finalPosition,1)
%     if YonGrid(i)==0
%         finalPosition(i,:)=0;
%     end
% end
% 
% rows_to_remove = any(finalPosition==0, 2);
% finalPosition(rows_to_remove,:) = [];
