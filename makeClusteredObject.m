function coords = makeClusteredObject(noPoints, varargin)
% Creates an n-by-3 list of coordinates distributed as a 3D Gaussain
% 
% Various input arguments are allowed as name-value pairs:
% 
% Half-width can be set with 'xWidth' and 'yWidth'.
% Spread can be set with 'sigmaX' and 'sigmaY'. These should be between 0
% and 1. Default value is 1; this means 99.994% of the points will lie
% inside the boundaries set by xWidth and yWidth. 
% 
% 
% 
% Marcus Fantham, 2015

%% Parse input
    p = inputParser;
    defaultWidth = 200;
    defaultDepth = 100;
    defaultOffset = 0;
    defaultZOffset = 100;
    defaultSigma = 1;
    defaultPlot = false;
    
    addRequired(p, 'noPoints', @isnumeric);
    addOptional(p, 'xWidth', defaultWidth, @isnumeric);
    addOptional(p, 'yWidth', defaultWidth, @isnumeric);
    addOptional(p, 'depth', defaultDepth, @isnumeric);
    addOptional(p, 'xOffset', defaultOffset, @isnumeric);
    addOptional(p, 'yOffset', defaultOffset, @isnumeric);
    addOptional(p, 'zOffset', defaultZOffset, @isnumeric);
    addOptional(p, 'sigmaX', defaultSigma, @isnumeric);
    addOptional(p, 'sigmaY', defaultSigma, @isnumeric);
    addOptional(p, 'sigmaZ', defaultSigma, @isnumeric);
    addOptional(p, 'plot', defaultPlot);
    
    parse(p,noPoints,varargin{:});
    
    xWidth = p.Results.xWidth;
    yWidth = p.Results.yWidth;
    depth = p.Results.depth;
    xOffset = p.Results.xOffset;
    yOffset = p.Results.yOffset;
    zOffset = p.Results.zOffset;
    sigmaX = p.Results.sigmaX;
    sigmaY = p.Results.sigmaY;
    sigmaZ = p.Results.sigmaZ;
    
%% 
% 0.006% chance of point lying outside xWidth or yWidth
x = xOffset + sigmaX * xWidth * randn(noPoints,1)/4;
y = yOffset + sigmaY * yWidth * randn(noPoints,1)/4;
z = (zOffset + depth/2) + sigmaZ * depth * randn(noPoints,1)/4;

x = round(x);
y = round(y);
z = round(z);

coords = [x, y, z];

%% Plot?
if p.Results.plot
    scatter3(coords(:,1), coords(:,2), coords(:,3));
    axis([-xWidth, xWidth, -yWidth, yWidth, zOffset, zOffset+depth]);
end

