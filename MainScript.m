%% Holography using the Rayleigh Sommerfeld diffraction formula
% Program calls functions to compute the hologram from a set of input
% parameters and then reconstructs said hologram.

close all; %clear all;
tic;

%% Load up the default parameters
parameterFile;

%% Load object coordinates
%objectCoord=[i-31,j-31,z];
%load('defaultObjectCoord.mat');
%
%% Random points on any level
load('randomObjectCoordinates');
noDistancesStored=noDistances;
noRandoms = 3000;                            % Number of random points. Choose to not use by setting equal to zero.
refDistance=parameters.refDistance;
holoHalfWidth=parameters.holoHalfWidth;
startDistance=parameters.startDistance;
endDistance=parameters.endDistance;
distanceRange = parameters.endDistance-parameters.startDistance;
noDistances = distanceRange/parameters.zPlanes+1;
refPlanes=parameters.refPlanes;
noObjectPointsStored=size(objectCoord,1)-(endDistance-startDistance)/refPlanes;
if noRandoms==0
    objectCoord=[0,0,1200];
end

% If the the number of random points, distance range or hologram halfwidth
% don't change, then the saved object coordinates are used.
if noRandoms~=0
    if noObjectPointsStored~=noRandoms || noDistances~=noDistancesStored || size(holo,1)-1~=holoHalfWidth*2
        objectCoord=zeros(noRandoms,3);
        m=1;
        for i=1:noRandoms
            objectCoord(m,:)=[randi([-(holoHalfWidth-refDistance-3),(holoHalfWidth-refDistance-3)]),randi([-(holoHalfWidth-refDistance-3),(holoHalfWidth-refDistance-3)]),randi([startDistance,endDistance])];
            m=m+1;
        end
    end
end
objectInfo=whos('objectCoord');
objectSize=objectInfo.bytes/1024;
fprintf('Input coordinate size is %.1f kb, ', objectSize);

%% Put reference points at a regular interval along the z axis.
if refPlanes~=0
    for z=1:(noDistances-1)/refPlanes
        dist=startDistance+z*refPlanes;
        referencePoint=[holoHalfWidth-refDistance,holoHalfWidth-refDistance,dist];
        objectCoord=[objectCoord;referencePoint];  % Add a reference peak at a large distance for thresholding
    end
end

%% Compute hologram. If it is equal to the stored hologram, use that.
if noObjectPointsStored~=noRandoms || noDistances~=noDistancesStored || size(holo,1)-1~=holoHalfWidth*2 || noRandoms==0
    holo=computeHologram(objectCoord);
    save('randomObjectCoordinates','objectCoord','noDistances','holo','noObjectPointsStored'); % Save all the new variables
end

switch 3
    case 1 % full hologram
        holo = filterHologram(holo, false, false ,false);
        [holo, compressedSize] = compressHologram(holo, objectSize, 'png');
    case 2 % quarter of the pixels
        holoS = holo(1:2:end, 1:2:end);
        [holoR, compressedSize] = compressHologram(holoS, objectSize, 'png');
        holo = zeros(size(holo));
        holo(1:2:end, 1:2:end) = holoR;
    case 3 % half of the pixels
        holoS = holo(1:2:end, :);
        [holoR, compressedSize] = compressHologram(holoS, objectSize, 'png');
        holo = zeros(size(holo));
        holo(1:2:end, :) = holoR;
    case 4 % a different half of the pixels
        holoS = holo(:, 1:2:end);
        [holoR, compressedSize] = compressHologram(holoS, objectSize, 'png');
        holo = zeros(size(holo));
        holo(:, 1:2:end) = holoR;
end

% figure(1)
% imshow(real(holo)/max(max(real(holo))))
% a=holo(holoHalfWidth,:);
% figure(2);
% plot(real(a));

%% Restore slices of the 3D space
recon=restoreSlices(holo);

%% Finds the peaks, only if a reference wave is input
if refPlanes~=0
    finalPosition = findPeaksFromSlices(recon);
    
    %% Calculates how accurately the reconstructed peaks are found
    difference=reconstructionAccuracy(objectCoord, finalPosition);
    
    %% Plot the restored peaks
    if size(finalPosition,1)<5*size(objectCoord,1)
        scatter3(finalPosition(:,1),finalPosition(:,2),finalPosition(:,3),'MarkerEdgeColor','k','MarkerFaceColor','r'); % Plot the determined z values
        hold on;
        scatter3(objectCoord(:,1),objectCoord(:,2),objectCoord(:,3))
    end
end
%implay(abs(recon)/max(max(max(abs(recon)))));
toc;