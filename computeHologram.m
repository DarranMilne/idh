function outputHologram = computeHologram(objectCoord)
%
% Functionalised version of George Roberts' code for computing a hologram
% from a point cloud.
% The reconstruction of the hologram which was in the original file has been 
% moved to  restorePointCloud.m, which is also a function. 

%% Parse the input arguments
if nargin == 1 && ~isempty(objectCoord)
    if ischar(objectCoord)
        if ~strcmp(objectCoord(end-3:end), '.mat')
            error('Input must be either a .mat file containing exactly one n x 3 coordinate matrix, or an n x 3 matrix.');
        end
        
        loader = load(objectCoord);
        fn = fieldnames(loader);
        if length(fn) ~= 1
            error('Input must be either a .mat file containing exactly one n x 3 coordinate matrix, or an n x 3 matrix.');
        end
        
        objectCoord = loader.(fn{1});
        if size(objectCoord, 2) ~= 3
            error('Input must be either a .mat file containing exactly one n x 3 coordinate matrix, or an n x 3 matrix.');
        end
    else
        if size(objectCoord, 2) ~= 3
            error('Input must be either an n x 3 matrix, or a .mat file containing exactly one n x 3 coordinate matrix.');
        end
    end
end

%% Load some parameters for our system
global parameters;

%% Create some useful variables
noPoints=size(objectCoord,1);              % Calculates the number of points
holoHW = parameters.holoHalfWidth;
k=parameters.k;
objectAmp=parameters.objectAmp;

objX = objectCoord(:,1); objY = objectCoord(:,2); objZ = objectCoord(:,3);
[planeY, planeX] = meshgrid(-holoHW : holoHW, -holoHW : holoHW);

%% Compute the hologram:
holo = 0;
for n = 1:noPoints
    dist = sqrt( (planeX-objX(n)).^2 + (planeY-objY(n)).^2 + (objZ(n)).^2 );
    holoM = objectAmp.* exp(1i.* k.* dist)./dist;
    holo = holo + holoM;  % note this won't work if we want to parallelise it later on; need to preallocate a : by : by n matrix, slot this in and then sum over 3rd dimension. 
end
%% Return the hologram
outputHologram = holo;