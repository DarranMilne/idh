function filteredHolo = filterHologram(inputHologram, checkerboard, innerCircleRadius, outerCircleRadius)
% M. Fantham 2015
%
% Filters the hologram. 
%
% Usage is as follows:
%  Input argument 1: The hologram you want to apply a filter to
%  Input argument 2: Logical - true if you want to apply a checkerboard 
%   filter, false if not. 
%  Input argument 3: Logical/double: false if no inner-circle filter, true
%   for an inner-circle filter 33% of the hologram size, double for a
%   filter of radius innerCircleRadius
%  Input argument 4: Logical/double: false if no outer-circle filter, true
%   for an outer-circle filter 75% of the hologram size, double for a
%   filter of radius outerCircleRadius

holo = inputHologram;

h = size(holo,1);
w = size(holo,2);
centerH = floor(h/2);
centerW = floor(w/2);
[W,H] = meshgrid(1:w,1:h);

% Apply filters
if checkerboard
    holo(1:2:end, 1:2:end) = 0;
    holo(2:2:end, 2:2:end) = 0;
end

if innerCircleRadius
    if islogical(innerCircleRadius)
        innerCircleRadius = 0.33 * size(holo,2)/2;
    end
    mask = sqrt((W-centerW).^2 + (H-centerH).^2) < innerCircleRadius;
    holo(mask) = 0;
end

if outerCircleRadius
    if islogical(innerCircleRadius)
        outerCircleRadius = 0.75 * size(holo,2)/2;
    end
    mask = sqrt((W-centerW).^2 + (H-centerH).^2) > outerCircleRadius;
    holo(mask) = 0;
end

filteredHolo = holo;