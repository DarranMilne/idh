function [compressedHologram, compressedSize] = compressHologram(holo, objectSize, compressionScheme)
%% Print pre-compression size
holo=single(holo);
holoInfo=whos('holo');
holoSize=holoInfo.bytes;
fprintf('initial hologram %.1f kb; ', holoSize/1024);

%% Save real an imaginary parts of hologram separately and scale so imwrite can read it
holoRe = real(holo);
holoReMin=min(min(holoRe));
holoRe=holoRe-holoReMin;        % Shift everything into the positive
holoReMax=max(max(holoRe));
holoRe=holoRe/holoReMax;        % Scale everything between 0 and 1
holoIm = imag(holo);            % Repeat for imaginary
holoImMin=min(min(holoIm));     
holoIm=holoIm-holoImMin;
holoImMax=max(max(holoIm));
holoIm=holoIm/holoImMax;

imwrite(holoRe, ['hologramRe.' compressionScheme], compressionScheme, 'BitDepth', 2);
imwrite(holoIm, ['hologramIm.' compressionScheme], compressionScheme, 'BitDepth', 2);

%% If pngcrush is installed, use this to try to compress the file even further
if system('pngcrush 1>OUTPUT 2>OUTPUT') == 1
    % Print some info pre-pngcrush
    compressedInfoRe = dir(['hologramRe.' compressionScheme]);
    compressedInfoIm = dir(['hologramIm.' compressionScheme]);
    compressedSize = (compressedInfoRe.bytes + compressedInfoIm.bytes)/1024;
    fprintf('\n before pngcrush, compressed hologram is %.1f kB; after pngcrush ', compressedSize);
    
    % Use pngcrush (note pngcrush can't overwrite)
    str = ['pngcrush -rem alla -rem tRNS -reduce -brute ' pwd '/hologramRe.png ' pwd '/hologramRe2.png 1>OUTPUT 2>OUTPUT'];
    system(str);
    str = ['pngcrush -rem alla -rem tRNS -reduce -brute ' pwd '/hologramIm.png ' pwd '/hologramIm2.png 1>OUTPUT 2>OUTPUT'];
    system(str);
    
    % Tidy up file names
    delete hologramRe.png hologramIm.png
    movefile('hologramRe2.png', 'hologramRe.png');
    movefile('hologramIm2.png', 'hologramIm.png');
end
delete('OUTPUT'); % clean up after yourself

%% Read in compressed hologram from file
clear holoRe holoIm
holoRe = single(imread(['hologramRe.' compressionScheme]));
holoIm = single(imread(['hologramIm.' compressionScheme]));

%% Rescale the values
holoRe=holoRe/255; 
holoIm=holoIm/255;
holoRe=holoRe*holoReMax;
holoIm=holoIm*holoImMax;
holoRe=holoRe+holoReMin;
holoIm=holoIm+holoImMin;

holo = complex(holoRe,holoIm);

%% Print compressed size
compressedInfoRe = dir(['hologramRe.' compressionScheme]);
compressedInfoIm = dir(['hologramIm.' compressionScheme]);
compressedSize = (compressedInfoRe.bytes + compressedInfoIm.bytes)/1024;
fprintf('compressed hologram is %.1f kB\n', compressedSize);
fprintf('The compression ratio is %.4f and',objectSize/compressedSize);

%% Return hologram 
compressedHologram = (holo);

